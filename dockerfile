FROM registry.gitlab.com/creditok/docker-nginx-php-laravel/master
COPY ./ /var/www/html/
RUN chown -R www-data /var/www/html/storage/